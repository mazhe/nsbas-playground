/*************************************************************************
 *
 *  This file is part of NSBAS.
 *
 *  NSBAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NSBAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

#include <complex.h>
#include <stdio.h>
#include <stdlib.h>

#include <gdal.h>

#include <nsbas/xmalloc.h>
#include <nsbas/xgdal.h>

int
main(int argc, char *argv[])
{
	char *nsb_look_path;
	char *look_func;
	int rlooks, alooks;
	char *test_file_path, *res_file_path, *ref_file_path;
	char nsb_look_cmd[4096];
	GDALDatasetH res_ds, ref_ds;
	GDALRasterBandH res_band, ref_band;
	int xsize, ysize;
	complex float *res_buf, *ref_buf;

	if (argc != 7) {
		fprintf(stderr, "error: wrong number of inputs\n");
		exit(EXIT_FAILURE);
	}
	nsb_look_path = argv[1];
	look_func = argv[2];
	rlooks = strtol(argv[3], NULL, 10);
	alooks = strtol(argv[4], NULL, 10);
	test_file_path = argv[5];
	ref_file_path = argv[6];
	res_file_path = "./test_look.tiff";

	/* Load GDAL drivers */
    GDALAllRegister();

	/* First, run nsb_look */
	snprintf(nsb_look_cmd, sizeof(nsb_look_cmd),
			"%s -f gtiff -l %s -a %d -r %d %s %s", 
			nsb_look_path, look_func, alooks, rlooks, test_file_path, res_file_path);
	if (system(nsb_look_cmd) != 0) {
		fprintf(stderr, "error: nsb_look failed\n");
		return EXIT_FAILURE;
	}

	/* Then compare output to what is expected */
	res_ds = xGDALOpen(res_file_path, GA_ReadOnly);
	res_band = GDALGetRasterBand(res_ds, 1);
	ref_ds = xGDALOpen(ref_file_path, GA_ReadOnly);
	ref_band = GDALGetRasterBand(ref_ds, 1);
	ysize = GDALGetRasterYSize(res_ds);
	xsize = GDALGetRasterXSize(res_ds);
	res_buf = xmalloc(xsize*sizeof(complex float));
	ref_buf = xmalloc(xsize*sizeof(complex float));
	for (int y = 0; y < ysize; y++) {
		xGDALRasterIO(
			res_band,
			GF_Read,
			0, y,
			xsize, 1,
			res_buf,
			xsize, 1,
			GDT_CFloat32,
			0, 0);
		xGDALRasterIO(
			ref_band,
			GF_Read,
			0, y,
			xsize, 1,
			ref_buf,
			xsize, 1,
			GDT_CFloat32,
			0, 0);
		for (int x = 0; x < xsize; x++) {
			double r_err, i_err;
			r_err = fabs(creal(res_buf[x]) - creal(ref_buf[x]));
			i_err = fabs(cimag(res_buf[x]) - cimag(ref_buf[x]));
			if (r_err > 0.00001 || i_err > 0.00001) {
				fprintf(stderr, 
						"error: pixel %d x %d has an absolute error of (%f, %f)\n",
						x, y, r_err, i_err);
				exit(1);
			}
		}
	}
	free(res_buf);
	GDALClose(res_ds);
	free(ref_buf);
	GDALClose(ref_ds);

	return EXIT_SUCCESS;
}
