      PROGRAM TEST_GDALF
      USE GDAL

      CHARACTER(len=1024) :: inputpath
      TYPE(c_ptr) :: ds, band
      INTEGER :: xsize, ysize

      CALL GET_COMMAND_ARGUMENT(1, inputpath)

      CALL GDALAllRegister()

      ds = GDALOpen(inputpath, 0)
      IF (.NOT. C_ASSOCIATED(ds)) THEN
        STOP 1
      END IF

      band = GDALGetRasterBand(ds, 1)
      IF (.NOT. C_ASSOCIATED(band)) THEN
        STOP 1
      END IF

      xsize = GDALGetRasterXSize(ds)
      IF (xsize /= 64) THEN
        WRITE (*,*) "GDALGetRasterXSize() failed"
      END IF
      ysize = GDALGetRasterYSize(ds)
      IF (xsize /= 64) THEN
        WRITE (*,*) "GDALGetRasterYSize() failed"
      END IF

      END PROGRAM TEST_GDALF
