/*************************************************************************
 *
 *  This file is part of NSBAS.
 *
 *  NSBAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NSBAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <gdal.h>
#include <gdal_alg.h>
#include <cpl_string.h>

#include <nsbas/xmalloc.h>
#include <nsbas/xgdal.h>

static void
usage(void)
{
	fprintf(stderr, "usage: nsb_radarlatlon [-f format] [-x] radar_file lookup_file dst_file\n");
}

int
main(int argc, char *argv[])
{
	bool fillholes = 0;
	const char *format = "roi_pac";
	GDALDatasetH sim_ds, src_ds, dst_ds;
	double src_geotransform[6];
	char **src_md;
	GDALRasterBandH src_ra_band, src_az_band, dst_lat_band, dst_lon_band;
	int src_xsize, src_ysize, dst_xsize, dst_ysize;
	int src_xmin, src_xmax, src_ymin, src_ymax;
	float *src_ra_buf, *src_az_buf, *dst_lat_buf, *dst_lon_buf;
	
	/* Load GDAL drivers */
	GDALAllRegister();
	
	/* Read arguments */
	while (1) {
		int c = getopt(argc, argv, "f:xh");
		if (c == -1) break;
		switch (c) {
		case 'f':
			format = optarg;
			break;
		case 'x':
			fillholes = true;
			break;
		case 'h':
			usage();
			exit(EXIT_SUCCESS);
		case ':':
			fprintf(stderr,
					"Option -%c requires an operand\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		case '?':
			fprintf(stderr,
					"Unrecognized option: -%c\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		}
	}
	if (argc < optind+3) {
		exit(EXIT_FAILURE);
	}

	/* Open radar */
	sim_ds = xGDALOpen(argv[optind], GA_ReadOnly);
	dst_xsize = GDALGetRasterXSize(sim_ds);
	dst_ysize = GDALGetRasterYSize(sim_ds);

	/* Open lookup table dataset */
	src_ds = xGDALOpen(argv[optind+1], GA_ReadOnly);
	if (GDALGetRasterCount(src_ds) != 2) {
		fprintf(stderr, "lookup image number of bands != 2\n");
		exit(EXIT_FAILURE);
	}
	GDALGetGeoTransform(src_ds, src_geotransform);
	src_ra_band = GDALGetRasterBand(src_ds, 1);
	src_az_band = GDALGetRasterBand(src_ds, 2);
	src_xsize = GDALGetRasterXSize(src_ds);
	src_ysize = GDALGetRasterYSize(src_ds);

	/* Use the metadata to get a better idea of where the non-zero values
	 * are in the image */
	src_xmin = src_ymin = 0;
	src_xmax = src_xsize;
	src_ymax = src_ysize;
	src_md = GDALGetMetadata(src_ds, "ROI_PAC");
	if (src_md != NULL) {
		src_xmin = strtol(CSLFetchNameValue(src_md, "XMIN"), NULL, 10);
		src_xmax = strtol(CSLFetchNameValue(src_md, "XMAX"), NULL, 10);
		src_ymin = strtol(CSLFetchNameValue(src_md, "YMIN"), NULL, 10);
		src_ymax = strtol(CSLFetchNameValue(src_md, "YMAX"), NULL, 10);
	}

	/* Create output dataset */
	dst_ds = xGDALCreate(GDALGetDriverByName(format), argv[optind+2], dst_xsize, dst_ysize, 2, GDT_Float32, NULL);
	dst_lat_band = GDALGetRasterBand(dst_ds, 1);
	dst_lon_band = GDALGetRasterBand(dst_ds, 2);
	GDALSetRasterNoDataValue(dst_lat_band, 0.);
	GDALSetRasterNoDataValue(dst_lon_band, 0.);
	//GDALFillRaster(dst_band, 0., 0.);

	/* Map the input points into outputs */
	src_ra_buf = xmalloc((src_xmax - src_xmin)*sizeof(float));
	src_az_buf = xmalloc((src_xmax - src_xmin)*sizeof(float));
	dst_lat_buf = xmalloc(dst_xsize*dst_ysize*sizeof(float));
	dst_lon_buf = xmalloc(dst_xsize*dst_ysize*sizeof(float));
	for (int y = src_ymin; y < src_ymax; y++) {
		/* Fetch lookup data */
		xGDALRasterIO(
			src_ra_band,
			GF_Read,
			src_xmin, y,
			src_xmax-src_xmin, 1,
			src_ra_buf,
			src_xmax-src_xmin, 1,
			GDT_Float32,
			0, 0);
		xGDALRasterIO(
			src_az_band,
			GF_Read,
			src_xmin, y,
			src_xmax-src_xmin, 1,
			src_az_buf,
			src_xmax-src_xmin, 1,
			GDT_Float32,
			0, 0);
		/* Now, loop over pixels in that line and for each one, set the
		 * right pixel in output array */
		#if defined(_OPENMP)
		# pragma omp parallel for
		#endif
		for (int x = 0; x < src_xmax-src_xmin; x++) {
			int dstx = src_ra_buf[x]-1, dsty = src_az_buf[x]-1; 
			if (dstx < 0 || dstx >= dst_xsize || dsty < 0 || dsty >= dst_ysize) continue;
			dst_lat_buf[dsty*dst_xsize + dstx] = src_geotransform[3] + y*src_geotransform[5];
			dst_lon_buf[dsty*dst_xsize + dstx] = src_geotransform[0] + (src_xmin+x)*src_geotransform[1];
		}
		/* Nothing to write now */
	}
	/* Now the image is ready, write it */
	xGDALRasterIO(
			dst_lat_band,
			GF_Write,
			0, 0,
			dst_xsize, dst_ysize,
			dst_lat_buf,
			dst_xsize, dst_ysize,
			GDT_Float32,
			0, 0);
	xGDALRasterIO(
			dst_lon_band,
			GF_Write,
			0, 0,
			dst_xsize, dst_ysize,
			dst_lon_buf,
			dst_xsize, dst_ysize,
			GDT_Float32,
			0, 0);
	/* Free the buffers */
	free(src_az_buf);
	free(src_ra_buf);
	free(dst_lat_buf);
	free(dst_lon_buf);

	/* Finally, fill the last nodata values */
	if (fillholes) {
		GDALFillNodata(dst_lat_band, NULL, 8.0, 0, 0, NULL, NULL, NULL);
		GDALFillNodata(dst_lon_band, NULL, 8.0, 0, 0, NULL, NULL, NULL);
	}

	/* Close datasets */
	GDALClose(dst_ds);
	GDALClose(src_ds);

	return EXIT_SUCCESS;
}
