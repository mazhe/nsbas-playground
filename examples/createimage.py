#!@PYTHON_EXECUTABLE@
# -*- coding: utf-8 -*-
##########################################################################
# 
#   This file is part of NSBAS.
# 
#   NSBAS is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   NSBAS is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
# 
##########################################################################

from __future__ import print_function
import sys

import numpy as np
import gdal

# Get a driver for the image format we want to use
drv = gdal.GetDriverByName("gtiff")

# Create the dataset using the driver
ds = drv.Create(sys.argv[1],      # path
                512,              # xsize
                256,              # ysize
                1,                # number of bands
                gdal.GDT_Float32) # data type
if not ds:
    exit(1)

# After this, dataset can be manipulated as usual
# Get its band
ds_band = ds.GetRasterBand(1)

# We will fill the band with data, line by line
for y in range(ds.RasterYSize):
    # Create random data
    data = np.random.rand(1, ds.RasterXSize)
    # Write this data at the desired point in the band (begining of y-th line)
    ds_band.WriteArray(data, 0, y)

# Deleting the dataset trigger the file close, which trigger writing any
# pending data
del ds 
