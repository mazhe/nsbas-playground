#!/usr/bin/env python
# -*- coding: utf-8 -*-
##########################################################################
# 
#   This file is part of NSBAS.
# 
#   NSBAS is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   NSBAS is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
# 
##########################################################################

from __future__ import print_function
import os, sys, tempfile

import numpy as np
import gdal

drv = gdal.GetDriverByName("gtiff")
drv2 = gdal.GetDriverByName("jpeg")

slc_ds = gdal.Open(sys.argv[1], gdal.GA_ReadOnly)
slc_band = slc_ds.GetRasterBand(1)
slc_min, slc_max, slc_mean, slc_stddev = slc_band.GetStatistics(0, 1)
slc_min = slc_mean - slc_stddev*2.0
slc_max = slc_mean + slc_stddev*2.0
slc_md = slc_ds.GetMetadata("ROI_PAC")
pixel_ratio = 1
if slc_md["ALOOKS"] != "1":
    pass
elif slc_md["PLATFORM"] == "Envisat":
    pixel_ratio = 5

tmpf = tempfile.NamedTemporaryFile()
out_ds = drv.Create(tmpf.name,
                    slc_band.XSize,
                    slc_band.YSize/pixel_ratio,
                    1, 
                    gdal.GDT_Byte)
out_band = out_ds.GetRasterBand(1)

for y in range(out_band.YSize):
    # TODO: use BandRasterIONumPy(), see gdal_array.py to see usage
    slc_line = slc_band.ReadAsArray(0, y*pixel_ratio,
                                    slc_band.XSize, pixel_ratio,
                                    slc_band.XSize, 1)
    out_line = (np.absolute(slc_line) - slc_min)*255./(slc_max - slc_min)
    out_band.WriteArray(out_line, 0, y)

del slc_ds

out_ds2 = drv2.CreateCopy(sys.argv[2], out_ds, 0, ["QUALITY=40"])
del out_ds
tmpf.close()
del out_ds2
