#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##########################################################################
# 
#   This file is part of NSBAS.
# 
#   NSBAS is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   NSBAS is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
# 
##########################################################################

from __future__ import print_function
import os, sys

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import gdal

# Initialize a matplotlib figure
fig, ax = plt.subplots(1)

# Open dataset (image)
ds = gdal.Open(sys.argv[1], gdal.GA_ReadOnly)

# Ok, assume this is a roipac image, so we can rely on the file extension
# to know how to display the data (in the real world, we would probably write
# different programs)
ds_extension = os.path.splitext(sys.argv[1])[1]

# Get the band that have the data we want
if ds_extension == ".unw":
    ds_band = ds.GetRasterBand(2)
else:
    ds_band = ds.GetRasterBand(2)

# Read data in numpy array, resampled by a factor of 4
# Note: Resampling is nearest neighbour with that function: hardly
#       acceptable, but easy for a simple demo!
data = ds_band.ReadAsArray(0, 0,
                           ds.RasterXSize, ds.RasterYSize,
                           ds.RasterXSize/4, ds.RasterYSize/4)
#data = np.nan_to_num(data)

if ds_extension == ".slc":
    # SLC, display amplitude
    data = np.absolute(data)
    ax.imshow(data, cm.Greys_r, vmax=np.percentile(data, 95))
elif ds_extension in [".int", ".flat"]:
    # Wrapped interferogram, display computed phase
    data_amp = np.absolute(data)
    data_pha = np.angle(data)
    ax.imshow(data_amp, cm.Greys_r, vmax=np.percentile(data, 95))
    ax.imshow(data_pha, cm.gist_rainbow, alpha=0.6)
elif ds_extension == ".hgt":
    # DEM in radar geometry
    ax.imshow(data, cm.Greys_r, vmax=np.percentile(data, 95))
elif ds_extension == ".unw":
    # Unwrapped inteferogram
    ax.imshow(data, cm.gist_rainbow, vmax=np.percentile(data, 98))
elif ds_extension ==  ".trans":
    # Geocoding lookup table
    ax.imshow(data)
else:
    ax.imshow(data, interpolation="bicubic")

# Close dataset (if this was a new image, it would be written at this moment)
# This will free memory for the display stuff
del ds

# Display the data
fig.canvas.set_window_title(sys.argv[1])
plt.show()
