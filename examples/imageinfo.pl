#!/usr/bin/env perl
##########################################################################
# 
#   This file is part of NSBAS.
# 
#   NSBAS is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   NSBAS is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
# 
##########################################################################

use strict;

use Geo::GDAL;

# Open dataset (image)
my $ds = Geo::GDAL::Open($ARGV[0]);

# Attributes
print "> Driver:   ".$ds->GetDriver()->{ShortName}."\n";
print "> Size:     ".$ds->{RasterXSize}."x".$ds->{RasterYSize}."x".$ds->{RasterCount}."\n";
print "> Datatype: ".$ds->GetRasterBand(1)->DataType()."\n";

# Metadata
my $ds_metadata = $ds->GetMetadata("ROI_PAC"); # We want ROI_PAC specifics
foreach my $m (keys %$ds_metadata) {
	print " - ".$m." : ".%$ds_metadata{$m}."\n";
}
