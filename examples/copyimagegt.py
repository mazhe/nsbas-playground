#!@PYTHON_EXECUTABLE@
# -*- coding: utf-8 -*-
##########################################################################
# 
#   This file is part of NSBAS.
# 
#   NSBAS is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   NSBAS is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
# 
##########################################################################

from __future__ import print_function
import sys

import gdal

# Open datasets (images)
in_ds = gdal.Open(sys.argv[1], gdal.GA_ReadOnly)
if in_ds == None:
    exit(1)
out_ds = gdal.Open(sys.argv[2], gdal.GA_Update)
if out_ds == None:
    exit(1)

# Copy coordinate system
out_ds.SetProjection(in_ds.GetProjection())

# Copy affine geotransform
out_ds.SetGeoTransform(in_ds.GetGeoTransform())

