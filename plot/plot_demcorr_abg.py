#!@PYTHON_EXECUTABLE@
# -*- coding: utf-8 -*-
##########################################################################
# 
#   This file is part of NSBAS.
# 
#   NSBAS is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   NSBAS is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
# 
##########################################################################

from __future__ import print_function
import getopt, posixpath as path, sys, warnings

import numpy as np
import gdal
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rc("axes", titlesize="medium")

fic_alpha_name = "fic_alpha"
fic_alpha_scale = 0.5
fic_beta_name = "fic_beta"
fic_beta_scale = 0.5
fic_gamma_name = "fic_gamma"
fic_gamma_scale = 0.5


opts, argv = getopt.getopt(sys.argv[1:], "fr")
for o, a in opts:
    if o == "-r":
        fic_alpha_name = "fic_alpha_cor_raboute"
        fic_alpha_scale = 1
        fic_beta_name = "fic_beta_cor_raboute"
        fic_beta_scale = 1
        fic_gamma_name = "fic_gamma_cor_raboute"
        fic_gamma_scale = 1
    elif o == "-f":
        fic_alpha_name = "fic_alpha_filtree"
        fic_alpha_scale = 1
    else:
        pass
radar_path = argv[0]
srcdir = argv[1] if len(argv) > 0 else ""

fig, ax = plt.subplots(1, 4, sharex=True, sharey=True)
if srcdir != "":
    fig.canvas.set_window_title(path.basename(srcdir))
ax[0].set_title("DEM")
ax[1].set_title(fic_alpha_name)
ax[2].set_title(fic_beta_name)
ax[3].set_title(fic_gamma_name)

radar_ds = gdal.Open(radar_path, gdal.GA_ReadOnly)
alpha_ds = gdal.Open(path.join(srcdir, fic_alpha_name), gdal.GA_ReadOnly)
beta_ds = gdal.Open(path.join(srcdir, fic_beta_name), gdal.GA_ReadOnly)
gamma_ds = gdal.Open(path.join(srcdir, fic_gamma_name), gdal.GA_ReadOnly)

if radar_ds != None:
    radar_band2 = radar_ds.GetRasterBand(2)
    radar_data2 = radar_band2.ReadAsArray(0,
                                          0,
                                          radar_ds.RasterXSize,
                                          radar_ds.RasterYSize,
                                          radar_ds.RasterXSize,
                                          radar_ds.RasterYSize)
    radar_im2 = ax[0].imshow(radar_data2, interpolation="bicubic")
    fig.colorbar(radar_im2, ax=ax[0], orientation="horizontal", pad = 0.05)

alpha_band = alpha_ds.GetRasterBand(1)
alpha_data = alpha_band.ReadAsArray(0,
                                    0, 
                                    alpha_ds.RasterXSize,
                                    alpha_ds.RasterYSize,
                                    alpha_ds.RasterXSize*fic_alpha_scale,
                                    alpha_ds.RasterYSize*fic_alpha_scale)
alpha_min, alpha_max, alpha_mean, alpha_stddev = alpha_band.GetStatistics(0, 1)
alpha_min, alpha_max = alpha_mean - alpha_stddev*2.5, alpha_mean + alpha_stddev*2.5
alpha_im = ax[1].imshow(alpha_data, interpolation="lanczos", vmin=alpha_min, vmax=alpha_max)
fig.colorbar(alpha_im, ax=ax[1], orientation="horizontal", pad = 0.05)

if beta_ds != None:
    beta_band = beta_ds.GetRasterBand(1)
    beta_data = beta_band.ReadAsArray(0,
                                      0, 
                                      beta_ds.RasterXSize,
                                      beta_ds.RasterYSize,
                                      beta_ds.RasterXSize*fic_beta_scale,
                                      beta_ds.RasterYSize*fic_beta_scale)
    beta_min, beta_max, beta_mean, beta_stddev = beta_band.GetStatistics(0, 1)
    beta_min, beta_max = beta_mean - beta_stddev*2.0, beta_mean + beta_stddev*2.0
    beta_im = ax[2].imshow(beta_data, interpolation="lanczos", vmin=beta_min, vmax=beta_max)
    fig.colorbar(beta_im, ax=ax[2], orientation="horizontal", pad = 0.05)

if gamma_ds != None:
    gamma_band = gamma_ds.GetRasterBand(1)
    gamma_data = gamma_band.ReadAsArray(0,
                                        0, 
                                        gamma_ds.RasterXSize,
                                        gamma_ds.RasterYSize,
                                        gamma_ds.RasterXSize*fic_gamma_scale,
                                        gamma_ds.RasterYSize*fic_gamma_scale)
    gamma_im = ax[3].imshow(gamma_data, interpolation="bicubic")
    fig.colorbar(gamma_im, ax=ax[3], orientation="horizontal", pad = 0.05)

ax[0].set_adjustable("box-forced")
ax[1].set_adjustable("box-forced")
ax[2].set_adjustable("box-forced")
ax[3].set_adjustable("box-forced")

if mpl.get_backend().lower() in ['agg', 'macosx']:
    fig.set_tight_layout(True)
else:
    plt.tight_layout()
plt.show()
