#!@PYTHON_EXECUTABLE@
# -*- coding: utf-8 -*-
##########################################################################
# 
#   This file is part of NSBAS.
# 
#   NSBAS is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   NSBAS is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
# 
##########################################################################

from __future__ import print_function
import os, sys, warnings

import numpy as np
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import matplotlib.widgets as widgets
import gdal

meanw = 64

mpl.rc("axes", titlesize="medium")
mpl.rc("figure", facecolor="1", edgecolor="white")

gdal.SetConfigOption("GDAL_PAM_ENABLED", "YES")

def update_ax1(band):
    global ds, ax1, ax1_data, ax1_im, ax1_an
    # Open the wanted band, retrieve visualisable min/max
    ds_band = ds.GetRasterBand(band)
    if ds_band == None:
        raise Exception("Cannot retrieve band %d in dataset" % band)
    ds_min, ds_max, ds_mean, ds_stddev = ds_band.GetStatistics(0, 1)
    ds_min, ds_max = ds_mean - ds_stddev*2.0, ds_mean + ds_stddev*2.0
    # Read the band data
    ax1_data = ds_band.ReadAsArray(0, 0, ds.RasterXSize, ds.RasterYSize)
    ax1_data[ax1_data == ds_band.GetNoDataValue()] = np.nan
    # Then update the matplotlib image with it
    ax1_im.set_data(ax1_data)
    ax1_im.set_clim(vmin=ds_min, vmax=ds_max)
    ax1.set_title("Displacement map")

def update_ax2(band):
    global ds, ax2_label
    # Open the wanted band, retrieve name, but do not read anything
    ds_band = ds.GetRasterBand(band)
    if ds_band == None:
        raise Exception("Cannot retrieve band %d in dataset" % band)
    if "Band_%d"%band in ds.GetMetadata():
        ax2_label.set_text(ds.GetMetadata()["Band_%d"%band])
    else:
        ax2_label.set_text("Band %d"%band)

def update_ax3():
    global ax1, ax1_data, ax1_an, ax3
    # Compute the angle bewteen X axis and arrow
    if hasattr(ax1_an, "xyann"): # matplotlib >= 1.4
        xstart, ystart = ax1_an.xyann[0], ax1_an.xyann[1]
    else:
        xstart, ystart = ax1_an.xytext[0], ax1_an.xytext[1]
    xstop, ystop = ax1_an.xy[0], ax1_an.xy[1]
    if xstart < xstop:
        theta = np.arctan((ystop-ystart)/(xstop-xstart))
    elif xstart > xstop:
        theta = np.arctan((ystop-ystart)/(xstop-xstart)) + np.pi
    else:
        theta = np.arctan(0.) 
    # Which allow us to compute the step we take on X and Y axis for each 
    # distance equivalent to a pixel we advance along the arrow
    xstep, ystep = np.cos(theta), np.sin(theta)

    # Now we can follow the arrow path to create the profile
    ax3_data_mean = []
    ax3_data_min, ax3_data_max = [], []
    ax3_data_more_std, ax3_data_less_std = [], []
    x, y = xstart, ystart
    while int(x) != int(xstop) and int(y) != int(ystop):
        # Do a profile of meanw centered on the point
        vals = []
        for i in range(int(-meanw/2),int(meanw/2)):
            xi = np.clip(int(x+i*ystep), 0, ds.RasterXSize-1)
            yi = np.clip(int(y+i*xstep), 0, ds.RasterYSize-1)
            vals.append(ax1_data[yi, xi])
        with warnings.catch_warnings(): # needed for arrays of NaN
            warnings.simplefilter("ignore", category=RuntimeWarning)
            vals = np.array(vals)
            ax3_data_mean.append(np.nanmean(vals))
            ax3_data_min.append(np.nanmin(vals))
            ax3_data_max.append(np.nanmax(vals))
            std = np.nanstd(vals)
            ax3_data_more_std.append(ax3_data_mean[-1] + std)
            ax3_data_less_std.append(ax3_data_mean[-1] - std)
        x,y = x+xstep, y+ystep 

    # Clear and redraw ax2
    ax3.cla()
    ax3.fill_between(range(len(ax3_data_mean)),
                     ax3_data_max, ax3_data_min,
                     #ax3_data_more_std, ax3_data_less_std,
                     where=None, facecolor='grey', alpha=0.35,
                     interpolate=True)
    ax3.plot(ax3_data_mean)
    ax3.set_xlim(0, len(ax3_data_mean))
    ax3.set_title("Displacement profile")

# Open the time serie dataset
ds = gdal.Open(sys.argv[1], gdal.GA_ReadOnly)
if ds == None:
    exit(1)
if ds.GetDriver().ShortName == "ENVI":   # NSBAS time serie (most likely)
    pass
elif ds.GetDriver().ShortName == "HDF5": # GiANT time serie (most likely)
    del ds
    ds = gdal.Open("HDF5:\""+sys.argv[1]+"\"://recons", gdal.GA_ReadOnly)

# Prepare plot layout
fig = plt.figure(1) 
gs = gridspec.GridSpec(12, 2)
ax1 = plt.subplot(gs[0:11, 0]) # Displacement map
ax2 = plt.subplot(gs[11, 0])   # Slider
ax3 = plt.subplot(gs[:, 1])   # Displacement profile

# Initialize ax1
ax1_data = np.zeros((ds.RasterYSize, ds.RasterXSize))
ax1_im = ax1.imshow(ax1_data, cm.gist_rainbow)
ax1_an = ax1.annotate("",
                      xy=(0, 0), xycoords="data",
                      xytext=(0, ds.RasterYSize), textcoords="data",
                      arrowprops=dict(fc="w",alpha=0.5,width=8))
ax1_an.set_visible(False)
def format_coord(x, y):
    return "x=%1.4f, y=%1.4f, z=%1.4f" % (x, y, ax1_data[int(y), int(x)])
ax1.format_coord = format_coord
fig.colorbar(ax1_im, ax=ax1, orientation="horizontal", pad = 0.05)
update_ax1(1)

# Initialize ax2
def update(val):
    update_ax1(int(val))
    update_ax2(int(val))
    update_ax3()
    fig.canvas.draw()
if ds.RasterCount > 1:
    ax2.set_title("Time serie")
    ax2_slider = widgets.Slider(ax2, "", 1, ds.RasterCount+1, valinit=1, valfmt="%0.0f")
    ax2_slider.valtext.set_visible(False)
    ax2_slider.on_changed(update)
else:
    ax2.set_visible(False)
ax2_label = ax2.text(0.5, -0.1,
                     "",
                     horizontalalignment="center", verticalalignment="top",
                     transform=ax2.transAxes)
update_ax2(1)

# Initialize ax3
ax3.set_title("Displacement profile")

# Global event handler functions
def onpress(event):
    if event.inaxes != ax1:
        return
    if hasattr(ax1_an, "xyann"): # matplotlib >= 1.4
        ax1_an.xyann = (event.xdata, event.ydata)
    else:
        ax1_an.xytext = (event.xdata, event.ydata)
def onrelease(event):
    if event.inaxes != ax1:
        return
    ax1_an.xy = (event.xdata, event.ydata)
    ax1_an.set_visible(True)
    update_ax3()
    fig.canvas.draw()
# Register the event handler functions
fig.canvas.mpl_connect("button_press_event", onpress)
fig.canvas.mpl_connect("button_release_event", onrelease)
# Window title
fig.canvas.set_window_title(os.path.basename(sys.argv[1]))
# Plot is setup, show()
plt.show()
