Playground libraries
====================

libutils
--------

libutils provides functions that extands either the standard C library 
functionnality (like memory allocation or strings) or other libraries
used in the project, such as GDAL.

libutils is build as an internal static library, virtualy used in every 
NSBAS program.

nsbas/string2.h
^^^^^^^^^^^^^^^

.. c:function:: void strlstrip(char *s)

.. c:function:: void strrstrip(char *s)

nsbas/xmalloc.h
^^^^^^^^^^^^^^^

.. c:function:: void *xmalloc(size_t size)

   This function behave exactly like the standard malloc() function, except 
   that any error will cause the calling process to be terminated through
   abort().

   .. note::
      
      xmalloc() do not garantee the data to be initialized in any way.

   .. seealso::
     
      `malloc() definition in the Open Group Base Specifications <http://pubs.opengroup.org/onlinepubs/9699919799/functions/malloc.html>`_

nsbas/xgdal.h
^^^^^^^^^^^^^

.. c:function:: GDALDatasetH xGDALOpen(const char *filename, GDALAccess access)

   This function behave exactly like the GDALOpen() function from the GDAL
   library, except that the program will be terminated using the 
   exit(EXIT_FAILURE) call if the image could not be opened.

   .. seealso::
      
      `GDALOpen() function definition from the project website <http://www.gdal.org/gdal_8h.html#a6836f0f810396c5e45622c8ef94624d4>`_

.. c:function:: GDALDatasetH xGDALCreate(GDALDriverH hDriver, const char *pszFilename, int nXSize, int nYSize, int nBands, GDALDataType eBandType, char **papszOptions)

   This function behave exactly like the GDALCreate() function from the GDAL
   library, except that the program will be terminated using the 
   exit(EXIT_FAILURE) call if the image could not be opened.

   .. seealso::
      
      `GDALCreate() function definition from the project website <http://www.gdal.org/gdal_8h.html#a8b45c298a8e697b0e7e006c068f657a6>`_
