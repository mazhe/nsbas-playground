.. NSBAS documentation master file, created by
   sphinx-quickstart on Tue Apr 12 10:30:49 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NSBAS's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 2

   programs
   libraries



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

