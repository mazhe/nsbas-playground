Playground programs
===================

By default, all programs that output datasets should offer a *-f* argument
to select the format. The default format is *roi_pac* for now.

nsb_fixnan
----------

Read an image to replace non-finite values by zeros. If no output filename
is specified, the image will be updated in place.

.. program:: nsb_fixnan

.. option:: -f format

   Select an ouput format.

nsb_radarlatlon
---------------

Read a radar reference image (real or simulated) and a geo -> radar lookup 
table, such as ROI_PAC geomap.trans files, to produce a dataset in radar
geometry with two bands. For each pixel in the reference radar geometry, 
the first band give the latitude and the second the longitude.

.. program:: nsb_radarlatlon

.. option:: -f format

   Select an ouput format.

nsb_snaphu
----------

This is actually the `snaphu <http://web.stanford.edu/group/radar/softwareandlinks/sw/snaphu/>`_ 
program, built internaly by NSBAS, and renamed to avoid name conflicts. See
`snaphu man page <http://web.stanford.edu/group/radar/softwareandlinks/sw/snaphu/snaphu_man1.html>`_
for information on how to use snaphu.

.. note::

   snaphu sources are used without any kind of modification, especially
   regarding dataset formats support. You may have to convert your data
   to use snaphu, and you will not be able to select a specific output
   format.
