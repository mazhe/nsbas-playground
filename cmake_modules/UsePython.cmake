macro(ADD_PYTHON_SCRIPT target source)
  add_custom_command(
      OUTPUT ${EXECUTABLE_OUTPUT_PATH}/${target}
      COMMAND ${CMAKE_COMMAND} -DINPUT=${CMAKE_CURRENT_SOURCE_DIR}/${source} -DOUTPUT=${EXECUTABLE_OUTPUT_PATH}/${target} -P ${CMAKE_BINARY_DIR}/configure_python_script.cmake
      COMMAND chmod 755 ${EXECUTABLE_OUTPUT_PATH}/${target}
      DEPENDS ${source}
      COMMENT "Configuring Python script ${target}"
      VERBATIM)
  add_custom_target(configure_${target} ALL DEPENDS ${EXECUTABLE_OUTPUT_PATH}/${target})
endmacro()
