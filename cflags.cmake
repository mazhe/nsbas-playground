# Ensure we use C99
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99")

# Add POSIX extensions over C99
add_definitions(-D_POSIX_C_SOURCE=200809L)
if(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
  remove_definitions(-D_POSIX_C_SOURCE=200809L)
  add_definitions(-D__DARWIN_C_LEVEL=9000000)
endif(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
if(${CMAKE_SYSTEM_NAME} MATCHES "FreeBSD")
  add_definitions(-D__BSD_VISIBLE=1)
endif(${CMAKE_SYSTEM_NAME} MATCHES "FreeBSD")

# Compiler-specific enhanced build messages
if(CMAKE_C_COMPILER_ID MATCHES "Clang|GNU")
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra")
endif(CMAKE_C_COMPILER_ID MATCHES "Clang|GNU")
if(CMAKE_C_COMPILER_ID MATCHES "Intel")
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -w2")
endif(CMAKE_C_COMPILER_ID MATCHES "Intel")

# When using gcc, staticaly link libgcc to have more portable binaries
if(CMAKE_C_COMPILER_ID MATCHES "GNU")
  set(CMAKE_REQUIRED_FLAGS "${CMAKE_REQUIRED_FLAGS} -static-libgcc")
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -static-libgcc")
endif(CMAKE_C_COMPILER_ID MATCHES "GNU")

# On FreeBSD (only?), fortran libs are built against port's GCC newer libgcc,
# then if programs are linked against older base libgcc, the linker will
# fail.
if(${CMAKE_SYSTEM_NAME} STREQUAL "FreeBSD")
  # Where is libgcc
  set(LIBGCC_DIR "/usr/local/lib/gcc48")
  # Flags check_function_exists
  set(CMAKE_REQUIRED_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -L${LIBGCC_DIR} -Wl,-rpath=${LIBGCC_DIR}")
  # Flags library/exe linkage
  set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -L${LIBGCC_DIR} -Wl,-rpath=${LIBGCC_DIR}")
  set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -L${LIBGCC_DIR} -Wl,-rpath=${LIBGCC_DIR}")
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -L${LIBGCC_DIR} -Wl,-rpath=${LIBGCC_DIR}")
endif(${CMAKE_SYSTEM_NAME} STREQUAL "FreeBSD")
