add_custom_command(
    OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/docopt.py
    COMMAND ${CMAKE_COMMAND} -E copy ${nsbas_SOURCE_DIR}/contrib/docopt-0.6.2/docopt.py ${CMAKE_CURRENT_BINARY_DIR}/docopt.py)

add_custom_target(python_modules ALL DEPENDS docopt.py)

install(
    FILES ${CMAKE_CURRENT_BINARY_DIR}/docopt.py
    DESTINATION lib/python${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR}/site-packages/nsbas
    PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
