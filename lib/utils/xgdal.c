/*************************************************************************
 *
 *  This file is part of NSBAS.
 *
 *  NSBAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NSBAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

#include <stdlib.h>

#include <gdal.h>

GDALDatasetH
xGDALOpen(const char *pszFilename, GDALAccess eAccess)
{
	GDALDatasetH ds;
	if ((ds = GDALOpen(pszFilename, eAccess)) == NULL) {
		exit(EXIT_FAILURE);
	}
	return ds;
}

GDALDatasetH
xGDALCreate(GDALDriverH hDriver, const char *pszFilename, int nXSize, int nYSize, int nBands, GDALDataType eBandType, char **papszOptions)
{
	GDALDatasetH ds;
	if ((ds = GDALCreate(hDriver, pszFilename, nXSize, nYSize, nBands, eBandType, papszOptions)) == NULL) {
		exit(EXIT_FAILURE);
	}
	return ds;
}

void
xGDALRasterIO(
		GDALRasterBandH hRBand, GDALRWFlag eRWFlag,
		  int nDSXOff, int nDSYOff, int nDSXSize, int nDSYSize,
		  void *pBuffer, int nBXSize, int nBYSize, GDALDataType eBDataType,
		  int nPixelSpace, int nLineSpace)
{
	if (GDALRasterIO(hRBand, eRWFlag, nDSXOff, nDSYOff, nDSXSize, nDSYSize, pBuffer, nBXSize, nBYSize, eBDataType, nPixelSpace, nLineSpace) != 0) {
		exit(EXIT_FAILURE);
	}
}

void
xGDALRasterIOEx(
		GDALRasterBandH hRBand, GDALRWFlag eRWFlag,
		int nDSXOff, int nDSYOff, int nDSXSize, int nDSYSize,
		void * pBuffer, int nBXSize, int nBYSize,GDALDataType eBDataType,
		GSpacing nPixelSpace, GSpacing nLineSpace,
		GDALRasterIOExtraArg* psExtraArg)
{
	if (GDALRasterIOEx(hRBand, eRWFlag, nDSXOff, nDSYOff, nDSXSize, nDSYSize, pBuffer, nBXSize, nBYSize, eBDataType, nPixelSpace, nLineSpace, psExtraArg) != 0) {
		exit(EXIT_FAILURE);
	}
}
