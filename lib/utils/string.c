/*************************************************************************
 *
 *  This file is part of NSBAS.
 *
 *  NSBAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NSBAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

#include <ctype.h>
#include <string.h>

void
strlstrip(char *s)
{
	char *s2 = s;
	if (*s != ' ' && *s != '\t') {
		return;
	}
	while (*s2 != '\0' && isspace(*s2)) {
		s2++;
	}
	while (*s2 != '\0') {
		*s = *s2;
		s++, s2++;
	}
	*s = '\0';
}

void
strrstrip(char *s)
{
	char *s2;
	s2 = s + strlen(s) - 1;
	while (s2 >= s && isspace(*s2)) {
		*s2 = '\0';
		s2--;
	}
}
