!      http://fortranwiki.org/fortran/show/Generating+C+Interfaces
       MODULE GDAL
       USE ISO_C_BINDING
       IMPLICIT NONE

!      GDAL typedefs
       TYPE,BIND(C) :: GDALDatasetH
         PRIVATE
         TYPE(c_ptr) :: ptr = C_NULL_PTR
       END TYPE GDALDatasetH

!      GDAL enumerations
!      ENUM :: GDALAccess
!        ENUMERATOR :: GA_ReadOnly = 0, GA_Update = 1
!      END ENUM GDALAccess

       INTERFACE
!        void GDALAllRegister(void)
         SUBROUTINE cGDALAllRegister() &
             BIND(C,name="GDALAllRegister")
         END SUBROUTINE cGDALAllRegister

!        GDALDatasetH GDALOpen(const char *pszFilename, GDALAccess eAccess)
         TYPE(c_ptr) FUNCTION cGDALOpen(pszFilename, eAccess) &
             BIND(C,name="GDALOpen")
           USE ISO_C_BINDING
           IMPORT GDALDatasetH
           CHARACTER(len=1,kind=c_char), INTENT(in), VALUE :: &
             pszFilename
           INTEGER(kind=c_int), INTENT(in), VALUE :: eAccess
         END FUNCTION cGDALOpen

!        GDALRasterBandH GDALGetRasterBand(GDALDatasetH hDS, int nBandId)
         TYPE(c_ptr) FUNCTION cGDALGetRasterBand(hDS, nBandId) &
             BIND(C,name="GDALGetRasterBand")
           USE ISO_C_BINDING
           IMPORT GDALDatasetH
           TYPE(c_ptr), INTENT(in), VALUE :: hDS
           INTEGER(kind=c_int), INTENT(in), VALUE :: nBandId
         END FUNCTION cGDALGetRasterBand

!        int GDALGetRasterXSize(GDALDatasetH hDataset)	
         INTEGER(kind=c_int) FUNCTION cGDALGetRasterXSize(hDataset) &
             BIND(C,name="GDALGetRasterXSize")
           USE ISO_C_BINDING
           IMPORT GDALDatasetH
           TYPE(c_ptr), INTENT(in), VALUE :: hDataset
         END FUNCTION cGDALGetRasterXSize

!        int GDALGetRasterYSize(GDALDatasetH hDataset)	
         INTEGER(kind=c_int) FUNCTION cGDALGetRasterYSize(hDataset) &
             BIND(C,name="GDALGetRasterYSize")
           USE ISO_C_BINDING
           IMPORT GDALDatasetH
           TYPE(c_ptr), INTENT(in), VALUE :: hDataset
         END FUNCTION cGDALGetRasterYSize

!        char** GDALGetMetadata(GDALMajorObjectH hObject, const char *pszDomain)
         TYPE(c_ptr) FUNCTION GDALGetMetadata(hObject, pszDomain) &
             BIND(C,name="GDALGetMetadata")
           USE ISO_C_BINDING
           TYPE(c_ptr), INTENT(in), VALUE :: hObject
           CHARACTER(len=1,kind=c_char), INTENT(in), VALUE :: pszDomain
         END FUNCTION
       END INTERFACE 

       CONTAINS

!      void GDALAllRegister(void)
       SUBROUTINE GDALAllRegister()
         CALL cGDALAllRegister()
       END SUBROUTINE GDALAllRegister

!      GDALDatasetH GDALOpen(const char *pszFilename, GDALAccess eAccess)
       FUNCTION GDALOpen(pszFilename, eAccess) RESULT(hDS)
         CHARACTER(len=*), INTENT(in) :: pszFilename
         INTEGER, INTENT(in) :: eAccess
         TYPE(c_ptr) :: hDS
         hDS = cGDALOpen(TRIM(pszFilename)//CHAR(0), eAccess)
       END FUNCTION GDALOpen

!      GDALRasterBandH GDALGetRasterBand(GDALDatasetH hDS, int nBandId)
       FUNCTION GDALGetRasterBand(hDS, nBandId) RESULT(hBand)
         TYPE(c_ptr), INTENT(in) :: hDS
         INTEGER, INTENT(in) :: nBandId
         TYPE(c_ptr) :: hBand
         hBand = cGDALGetRasterBand(hDS, nBandId)
       END FUNCTION GDALGetRasterBand

!      int GDALGetRasterXSize(GDALDatasetH hDataset)
       FUNCTION GDALGetRasterXSize(hDataset) RESULT(iXSize)
         TYPE(c_ptr), INTENT(in) :: hDataset
         INTEGER :: iXSize
         iXSize = cGDALGetRasterXSize(hDataset)
       END FUNCTION GDALGetRasterXSize

!      int GDALGetRasterYSize(GDALDatasetH hDataset)	
       FUNCTION GDALGetRasterYSize(hDataset) RESULT(iYSize)
         TYPE(c_ptr), INTENT(in) :: hDataset
         INTEGER :: iYSize
         iYSize = cGDALGetRasterYSize(hDataset)
       END FUNCTION GDALGetRasterYSize

!      char** GDALGetMetadata(GDALMajorObjectH hObject, const char *pszDomain)
!      FUNCTION GDALGetMetadata(hObject, pszDomain) RESULT(a)
!        TYPE(c_ptr), INTENT(in) :: hObject
!        CHARACTER(len=*), INTENT(in) :: pszDomain
!      END FUNCTION GDALGetMetadata

       END MODULE
