/*************************************************************************
 *
 *  This file is part of NSBAS.
 *
 *  NSBAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NSBAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <gdal.h>

#include <nsbas/config.h>
#include <nsbas/xmalloc.h>
#include <nsbas/xgdal.h>

static void
usage(void)
{
	fprintf(stderr, "usage: nsb_rewrap [-f format] [-r rewraps] unw_file dst_file\n");
}

int
main(int argc, char *argv[])
{
	const char *format = NSBAS_DEFAULT_FORMAT;
	double rewrap = 2 * M_PI;
	char **dst_options;
	GDALDatasetH src_ds, dst_ds;
	GDALRasterBandH src_ampli_band, src_phase_band, dst_ampli_band, dst_phase_band;
	int xsize, ysize;
	float *ampli_buf, *phase_buf;
	
	/* Load GDAL drivers */
	GDALAllRegister();
	
	/* Read arguments */
	while (1) {
		int c = getopt(argc, argv, "f:r:h");
		if (c == -1) break;
		switch (c) {
		case 'f':
			format = optarg;
			break;
		case 'r':
			rewrap = strtod(optarg, NULL) * M_PI;
			break;
		case 'h':
			usage();
			exit(EXIT_SUCCESS);
		case ':':
			fprintf(stderr,
					"Option -%c requires an operand\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		case '?':
			fprintf(stderr,
					"Unrecognized option: -%c\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		}
	}
	if (argc < optind+1) {
		usage();
		exit(EXIT_FAILURE);
	}

	/* Open source dataset */
	src_ds = xGDALOpen(argv[optind], GA_ReadOnly);
	src_ampli_band = GDALGetRasterBand(src_ds, 1);
	src_phase_band = GDALGetRasterBand(src_ds, 2);

	/* Compute sizes for output dataset */
	xsize = GDALGetRasterXSize(src_ds);
	ysize = GDALGetRasterYSize(src_ds);

	/* Create output dataset */
	dst_ds = 
		xGDALCreate(
				GDALGetDriverByName(format),
				argv[optind+1],
				xsize, ysize, 2,
				GDT_Float32,
				NULL);
	dst_ampli_band = GDALGetRasterBand(dst_ds, 1);
	dst_phase_band = GDALGetRasterBand(dst_ds, 2);

	/* Do the downsampling */
	ampli_buf = xmalloc(xsize*sizeof(float));
	phase_buf = xmalloc(xsize*sizeof(float));
	for (int y = 0; y < ysize; y++) {
		xGDALRasterIO(
				src_ampli_band,
				GF_Read,
				0, y,
				xsize, 1,
				ampli_buf,
				xsize, 1,
				GDT_Float32,
				0, 0);
		xGDALRasterIO(
				src_phase_band,
				GF_Read,
				0, y,
				xsize, 1,
				phase_buf,
				xsize, 1,
				GDT_Float32,
				0, 0);
		for (int x = 0; x < xsize; x++) {
			phase_buf[x] = fmod(phase_buf[x], rewrap);
			if (phase_buf[x] < 0) {
				phase_buf[x] += rewrap;
			}
		}
		xGDALRasterIO(
				dst_ampli_band,
				GF_Write,
				0, y,
				xsize, 1, 
				ampli_buf,
				xsize, 1,
				GDT_Float32,
				0, 0);
		xGDALRasterIO(
				dst_phase_band,
				GF_Write,
				0, y,
				xsize, 1, 
				phase_buf,
				xsize, 1,
				GDT_Float32,
				0, 0);
	}
	free(ampli_buf);
	free(phase_buf);

	/* Close datasets */
	GDALClose(src_ds);
	GDALClose(dst_ds);

	return EXIT_SUCCESS;
}
