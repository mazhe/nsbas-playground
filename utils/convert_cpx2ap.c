/*************************************************************************
 *
 *  This file is part of NSBAS.
 *
 *  NSBAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NSBAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

#include <complex.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <gdal.h>

#include <nsbas/config.h>
#include <nsbas/xmalloc.h>
#include <nsbas/xgdal.h>

static void
usage(void)
{
	fprintf(stderr, "usage: nsb_cpx2ap [-f format] source_file destination_file\n");
}

int
main(int argc, char *argv[])
{
	const char *format = "roi_pac";
	GDALDatasetH src_ds, dst_ds;
	GDALRasterBandH src_band, dst_band1, dst_band2;
	int src_xsize, src_ysize, dst_xsize, dst_ysize;
	complex float *src_buf;
    float *dst_buf1, *dst_buf2;
	double geotransform[6];
	
	/* Load GDAL drivers */
	GDALAllRegister();
	
	/* Read arguments */
	while (1) {
		int c = getopt(argc, argv, "f:h");
		if (c == -1) break;
		switch (c) {
		case 'f':
			format = optarg;
			break;
		case 'h':
			usage();
			exit(EXIT_SUCCESS);
		case ':':
			fprintf(stderr,
					"Option -%c requires an operand\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		case '?':
			fprintf(stderr,
					"Unrecognized option: -%c\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		}
	}
	if (argc < optind+1) {
		exit(EXIT_FAILURE);
	}

	/* Open source dataset */
	src_ds = xGDALOpen(argv[optind], GA_ReadOnly);
	if (GDALGetRasterCount(src_ds) != 1) {
		fprintf(stderr, "source image has more than one band and/or is not complex\n");
		exit(EXIT_FAILURE);
    }
	src_band = GDALGetRasterBand(src_ds, 1);

	/* Compute sizes for output dataset */
	src_xsize = GDALGetRasterXSize(src_ds);
	src_ysize = GDALGetRasterYSize(src_ds);
	dst_xsize = src_xsize;
	dst_ysize = src_ysize;

	/* Create output dataset */
	dst_ds = GDALCreate(GDALGetDriverByName(format), argv[optind+1], dst_xsize, dst_ysize, 2, GDT_Float32, NULL);
	if (dst_ds == NULL) {
		exit(EXIT_FAILURE);
	}
	dst_band1 = GDALGetRasterBand(dst_ds, 1);
	dst_band2 = GDALGetRasterBand(dst_ds, 2);

	/* Do the conversion */
	src_buf = xmalloc(src_xsize*sizeof(complex float));
	dst_buf1 = xmalloc(dst_xsize*sizeof(float));
	dst_buf2 = xmalloc(dst_xsize*sizeof(float));
	for (int y = 0; y < dst_ysize; y++) {
		xGDALRasterIO(
				src_band,
				GF_Read,
				0, y,
				src_xsize, 1,
				src_buf,
				src_xsize, 1,
				GDT_CFloat32,
				0, 0);

		for (int x = 0; x < dst_xsize; x++) {
			dst_buf1[x] = cabsf(src_buf[x]);
			dst_buf2[x] = cargf(src_buf[x]);
		}

		xGDALRasterIO(
				dst_band1,
				GF_Write,
				0, y,
				dst_xsize, 1, 
				dst_buf1,
				dst_xsize, 1,
				GDT_Float32,
				0, 0);
		xGDALRasterIO(
				dst_band2,
				GF_Write,
				0, y,
				dst_xsize, 1, 
				dst_buf2,
				dst_xsize, 1,
				GDT_Float32,
				0, 0);
	}
	free(src_buf);
	free(dst_buf1);
	free(dst_buf2);

	/* Copy metadata */
	GDALGetGeoTransform(src_ds, geotransform);
	GDALSetGeoTransform(dst_ds, geotransform);
	GDALSetProjection(dst_ds, GDALGetProjectionRef(src_ds));
	GDALSetMetadata(dst_ds, GDALGetMetadata(src_ds, "ROI_PAC"), "ROI_PAC");

	/* Close datasets */
	GDALClose(src_ds);
	GDALClose(dst_ds);

	return EXIT_SUCCESS;
}
