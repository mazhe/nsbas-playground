/*************************************************************************
 *
 *  This file is part of NSBAS.
 *
 *  NSBAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NSBAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

#include <complex.h>
#include <stdlib.h>
#include <stdio.h>

#include <gdal.h>

#include <nsbas/config.h>
#include <nsbas/xmalloc.h>
#include <nsbas/xgdal.h>

static void
usage(void)
{
	fprintf(stderr, "usage: nsb_fixnan [-f format] source_file [destination_file]\n");
}

int
main(int argc, char *argv[])
{
	const char *format = NSBAS_DEFAULT_FORMAT;
	GDALDatasetH src_ds, dst_ds;
	int xsize, ysize, count;
	GDALDataType dtype;
	complex float *buf;
	
	/* Load GDAL drivers */
	GDALAllRegister();
	
	/* Read arguments */
	while (1) {
		int c = getopt(argc, argv, "f:h");
		if (c == -1) break;
		switch (c) {
		case 'f':
			format = optarg;
			break;
		case 'h':
			usage();
			exit(EXIT_SUCCESS);
		case ':':
			fprintf(stderr,
					"Option -%c requires an operand\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		case '?':
			fprintf(stderr,
					"Unrecognized option: -%c\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		}
	}
	if (argc < optind) {
		usage();
		exit(EXIT_FAILURE);
	}

	if (optind == argc-1) {
		/* Open dataset to edit in place */
		src_ds = xGDALOpen(argv[optind], GA_Update);
		dtype = GDALGetRasterDataType(GDALGetRasterBand(src_ds, 1));

		/* Compute sizes for output dataset */
		count = GDALGetRasterCount(src_ds);
		xsize = GDALGetRasterXSize(src_ds);
		ysize = GDALGetRasterYSize(src_ds);

		/* Since we're editing in place... */
		dst_ds = src_ds;
	}
	else {
		/* Open source dataset */
		src_ds = xGDALOpen(argv[optind], GA_ReadOnly);
		dtype = GDALGetRasterDataType(GDALGetRasterBand(src_ds, 1));

		/* Compute sizes for output dataset */
		count = GDALGetRasterCount(src_ds);
		xsize = GDALGetRasterXSize(src_ds);
		ysize = GDALGetRasterYSize(src_ds);

		/* Create output dataset */
		dst_ds = GDALCreate(GDALGetDriverByName(format), argv[optind+1], xsize, ysize, count, dtype, NULL);
		if (dst_ds == NULL) {
			exit(EXIT_FAILURE);
		}

		/* Copy metadata */
        GDALSetMetadata(dst_ds, GDALGetMetadata(src_ds, "ROI_PAC"), "ROI_PAC");
	}

	/* Do the byte swaping */
	buf = xmalloc(xsize*sizeof(complex float));
	for (int b = 1; b <= count; b++) {
		GDALRasterBandH src_band, dst_band;
		src_band = GDALGetRasterBand(src_ds, b);
		dst_band = GDALGetRasterBand(dst_ds, b);
		for (int y = 0; y < ysize; y++) {
			xGDALRasterIO(
					src_band,
					GF_Read,
					0, y,
					xsize, 1,
					buf,
					xsize, 1,
					GDT_CFloat32,
					0, 0);

			for (int x = 0; x < xsize; x++) {
				float r = crealf(buf[x]), i = cimagf(buf[x]);
				if (!isfinite(r)) {
					buf[x] = 0.f + I*i;
				}
				if (!isfinite(i)) {
					buf[x] = crealf(buf[x]);
				}
			}

			xGDALRasterIO(
					dst_band,
					GF_Write,
					0, y,
					xsize, 1, 
					buf,
					xsize, 1,
					GDT_CFloat32,
					0, 0);
		}
	}
	free(buf);

	/* Close datasets */
	GDALClose(src_ds);
	if (dst_ds != src_ds) {
		GDALClose(dst_ds);
	}

	return EXIT_SUCCESS;
}
