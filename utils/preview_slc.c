/*************************************************************************
 *
 *  This file is part of NSBAS.
 *
 *  NSBAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NSBAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

#include <complex.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#include <cpl_string.h>
#include <gdal.h>

#include <nsbas/config.h>
#include <nsbas/xmalloc.h>
#include <nsbas/xgdal.h>

void
usage(void)
{
	fprintf(stderr,
			"usage:\n"
				"  nsb_preview_slc slc_file out_file\n\n"
                "options:\n"
                "  -l nlooks: downsample image by nlooks\n"
                "  -p ratio: pixel ratio\n"
				"\n");
}

static inline void
look_pow(complex float *data, int xsize, int alooks, int rlooks)
{
    for (int x = 0; x < xsize; x++) {
        float z = cabsf(data[x]) * cabsf(data[x]);
        for (int i = 1; i < alooks; i++) {
            float f = cabsf(data[i*xsize + x]);
            z += f*f;
        }
        data[x] = z;
    }
    for (int x = 0; x < xsize/rlooks; x++) {
        float z = crealf(data[x*rlooks]);
        for (int i = 1; i < rlooks; i++) {
            z += crealf(data[x*rlooks + i]);
        }
        data[x] = z;
    }
}

int
main(int argc, char *argv[])
{
	char tmp_path[64] = "/tmp/nsb.XXXXXX";
	int tmp_fd;
	int looks = 1;
	char **dst_options;
	GDALDatasetH slc_ds, tmp_ds, dst_ds;
	char **slc_md;
	int pixel_ratio = 0;
	GDALRasterBandH slc_band, tmp_band;
	double slc_min, slc_max, slc_mean, slc_stddev;
	int slc_xsize, slc_ysize, dst_xsize, dst_ysize;
	complex float *slc_buf;

	/* Read arguments */
	while (1) {
		int c = getopt(argc, argv, "l:p:h");
		if (c == -1) break;
		switch (c) {
		case 'l':
			looks = strtod(optarg, NULL);
			break;
		case 'p':
			pixel_ratio = strtod(optarg, NULL);
			break;
		case 'h':
			usage();
			exit(EXIT_SUCCESS);
		case ':':
			fprintf(stderr,
					"Option -%c requires an operand\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		case '?':
			fprintf(stderr,
					"Unrecognized option: -%c\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		}
	}
	if (argc < optind+2) {
		usage();
		exit(EXIT_FAILURE);
	}

	/* Load GDAL drivers */
	GDALAllRegister();

	/* Open SLC dataset */
	slc_ds = xGDALOpen(argv[optind], GA_ReadOnly);
	slc_band = GDALGetRasterBand(slc_ds, 1);
	/* Choose a pixel ratio depending on sensor */
	slc_md = GDALGetMetadata(slc_ds, "ROI_PAC");
	if (pixel_ratio == 0) {
		if (strcmp(CSLFetchNameValue(slc_md, "PLATFORM"), "Envisat") == 0) {
			pixel_ratio = 5;
		}
		else if (strcmp(CSLFetchNameValue(slc_md, "PLATFORM"), "ALOS") == 0) {
			pixel_ratio = 3;
		}
		else {
			pixel_ratio = 1;
		}
	}
	/* Statistics in complex images only apply to real part, but that'll do */
	/* Note: we still have to adapt those statistics values to the fact we
     *       will downlook the image using the power method */
	GDALGetRasterStatistics(
			slc_band,
			1, 1,
			&slc_min, &slc_max, &slc_mean, &slc_stddev);
	slc_min = 0;
	slc_max = (slc_mean + slc_stddev*slc_stddev*4.0)*looks*looks*pixel_ratio;

	/* Get sizes for output dataset */
	slc_xsize = GDALGetRasterXSize(slc_ds);
	slc_ysize = GDALGetRasterYSize(slc_ds);
	dst_xsize = slc_xsize/looks;
	dst_ysize = slc_ysize/pixel_ratio/looks;

	/* Create temp dataset */
	tmp_fd = mkstemp(tmp_path);
	if (tmp_fd == -1) {
		fprintf(stderr, "cannot create tmp file\n");
		exit(EXIT_FAILURE);
	}
	unlink(tmp_path);
	tmp_ds = 
		GDALCreate(
				GDALGetDriverByName("gtiff"),
				tmp_path,
				dst_xsize, dst_ysize, 1,
				GDT_Byte,
				NULL);
	if (tmp_ds == NULL) {
		fprintf(stderr, "cannot open %s\n", argv[3]);
		exit(EXIT_FAILURE);
	}
	close(tmp_fd);
	tmp_band = GDALGetRasterBand(tmp_ds, 1);

	/* Do the stuff */
	slc_buf = xmalloc(slc_xsize*pixel_ratio*looks*sizeof(complex float));
	for (int y = 0; y < dst_ysize; y++) {
		xGDALRasterIO(
				slc_band,
				GF_Read,
				0, y*pixel_ratio*looks,
				slc_xsize, pixel_ratio*looks,
				slc_buf,
				slc_xsize, pixel_ratio*looks,
				GDT_CFloat32,
				0, 0);
		look_pow(slc_buf, slc_xsize, looks*pixel_ratio, looks);
		for (int x = 0; x < dst_xsize; x++) {
			slc_buf[x] = (crealf(slc_buf[x]) - slc_min)*(255.)/(slc_max - slc_min);
		}
		xGDALRasterIO(
				tmp_band,
				GF_Write,
				0, y,
				dst_xsize, 1,
				slc_buf,
				dst_xsize, 1,
				GDT_CFloat32,
				0, 0);
	}
	free(slc_buf);

	/* Close input datasets */
	GDALClose(slc_ds);

	/* Create the jpeg */
	dst_options = CSLSetNameValue(NULL, "QUALITY", "40");
	dst_ds =
		GDALCreateCopy(
				GDALGetDriverByName("jpeg"),
				argv[optind+1],
				tmp_ds,
				FALSE,
				dst_options,
				NULL, NULL);

	/* Close datasets */
	GDALClose(tmp_ds);
	GDALClose(dst_ds);

	/* Unlink tmp file again, it seems that gdal will reopen it at the first
	 * occasion */
	unlink(tmp_path);

	return EXIT_SUCCESS;
}

