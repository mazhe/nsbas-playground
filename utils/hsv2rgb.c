/*************************************************************************
 *
 *  This file is part of NSBAS.
 *
 *  NSBAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NSBAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

#include <math.h>
#include <stdint.h>

void 
hsv2rgb(uint8_t *r, uint8_t *g, uint8_t *b, float h, float s, float v)
{
	int i;
	float f, p, q, t;

	if (s == 0.f) {
		*r = *g = *b = v;
		return;
	}

	h *= 6.0f;
	i = floorf(h);
	f = h - i;
	p = v*(1.0f - s);
	q = v*(1.0f - s*f);
	t = v*(1.0f - s*(1.0f - f));

	switch (i) {
	case 0:
		*r = v; *g = t; *b = p;
		break;
	case 1:
		*r = q; *g = v; *b = p;
		break;
	case 2:
		*r = p; *g = v; *b = t;
		break;
	case 3:
		*r = p; *g = q; *b = v;
		break;
	case 4:
		*r = t; *g = p; *b = v;
		break;
	default: /* case 5: */
		*r = v; *g = p; *b = q;
		break;
	}
}
