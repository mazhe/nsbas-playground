/*************************************************************************
 *
 *  This file is part of NSBAS.
 *
 *  NSBAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NSBAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

#include <complex.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#include <cpl_string.h>
#include <gdal.h>

#include <nsbas/config.h>
#include <nsbas/xmalloc.h>
#include <nsbas/xgdal.h>

#include "hsv2rgb.h"

void
usage(void)
{
	fprintf(stderr,
			"usage:\n"
				"  nsb_preview_int [-c color_table] [-l nlooks] radar_file int_file out_file\n\n"
				"options:\n"
				"  -c color_table: apply a specific color lookup table (roipac, jet, bluered)\n"
				"  -l nlooks: downsample image by nlooks\n"
				"\n");
}

extern const float clut_bluered[];
extern const float clut_jet[];
extern const float clut_roipac[];

static inline void
look_cplx(complex float *data, int xsize, int alooks, int rlooks)
{
	for (int x = 0; x < xsize; x++) {
		complex float z = data[x];
		for (int i = 1; i < alooks; i++) {
			z += data[i*xsize + x];
		}
		data[x] = z;
	}
	for (int x = 0; x < xsize/rlooks; x++) {
		complex float z = data[x*rlooks];
		for (int i = 1; i < rlooks; i++) {
			z += data[x*rlooks + i];
		}
		data[x] = z;
	}
}

int
main(int argc, char *argv[])
{
	char tmp_path[64] = "/tmp/nsb.XXXXXX";
	int tmp_fd;
	int looks = 1;
	char **dst_options;
	GDALDatasetH radar_ds, int_ds, tmp_ds, dst_ds;
	GDALRasterBandH slp_band, int_band, tmp_band_r, tmp_band_g, tmp_band_b;
	double slp_min, slp_max, slp_mean, slp_stddev;
	double int_min, int_max;
	int radar_xsize, radar_ysize, int_xsize, int_ysize, xsize, ysize;
	float *slp_buf; complex float *int_buf;
	uint8_t *tmp_buf_r, *tmp_buf_g, *tmp_buf_b;
	const float *clut = clut_roipac;
	
	/* Load GDAL drivers */
	GDALAllRegister();

	/* Read arguments */
	while (1) {
		int c = getopt(argc, argv, "c:l:h");
		if (c == -1) break;
		switch (c) {
		case 'c':
			if (strcmp(optarg, "bluered") == 0) {
				clut = clut_bluered;
			}
			else if (strcmp(optarg, "jet") == 0) {
				clut = clut_jet;
			}
			else if (strcmp(optarg, "roipac") == 0) {
				clut = clut_roipac;
			}
			else {
				fprintf(stderr,
						"Unknown color lookup table '%s': use bluered or roipac\n", optarg);
				exit(EXIT_FAILURE);
			}
			break;
		case 'l':
			looks = strtol(optarg, NULL, 10);
			break;
		case 'h':
			usage();
			exit(EXIT_SUCCESS);
		case ':':
			fprintf(stderr,
					"Option -%c requires an operand\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		case '?':
			fprintf(stderr,
					"Unrecognized option: -%c\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		}
	}
	if (argc < optind+3) {
		usage();
		exit(EXIT_FAILURE);
	}

	/* Open relief dataset */
	radar_ds = xGDALOpen(argv[optind], GA_ReadOnly);
	slp_band = GDALGetRasterBand(radar_ds, 1);
	GDALGetRasterStatistics(
			slp_band,
			1, 1,
			&slp_min, &slp_max, &slp_mean, &slp_stddev);
	slp_min = slp_mean - slp_stddev*2.0;
	slp_max = slp_mean + slp_stddev*2.0;

	/* Open int dataset */
	int_ds = xGDALOpen(argv[optind+1], GA_ReadOnly);
	int_band = GDALGetRasterBand(int_ds, 1);
	int_min = -M_PI, int_max = +M_PI;

	/* Get sizes for output dataset */
	radar_xsize = GDALGetRasterXSize(radar_ds);
	radar_ysize = GDALGetRasterYSize(radar_ds);
	int_xsize = GDALGetRasterXSize(int_ds);
	int_ysize = GDALGetRasterYSize(int_ds);
	xsize = int_xsize;
	ysize = MIN(int_ysize, radar_xsize/int_xsize*radar_ysize);

	/* Create temp dataset */
	tmp_fd = mkstemp(tmp_path);
	if (tmp_fd == -1) {
		fprintf(stderr, "cannot create tmp file\n");
		exit(EXIT_FAILURE);
	}
	unlink(tmp_path);
	tmp_ds =
		GDALCreate(
				GDALGetDriverByName("gtiff"),
				tmp_path,
				xsize, ysize, 3,
				GDT_Byte,
				NULL);
	if (tmp_ds == NULL) {
		fprintf(stderr, "cannot open %s\n", argv[3]);
		exit(EXIT_FAILURE);
	}
	close(tmp_fd);
	tmp_band_r = GDALGetRasterBand(tmp_ds, 1);
	tmp_band_g = GDALGetRasterBand(tmp_ds, 2);
	tmp_band_b = GDALGetRasterBand(tmp_ds, 3);

	/* Do the stuff */
	slp_buf = xmalloc(xsize*sizeof(float));
	int_buf = xmalloc(xsize*sizeof(complex float));
	tmp_buf_r = xmalloc(xsize*sizeof(uint8_t));
	tmp_buf_g = xmalloc(xsize*sizeof(uint8_t));
	tmp_buf_b = xmalloc(xsize*sizeof(uint8_t));
	for (int y = 0; y < ysize; y++) {
		int slp_y = MIN(MAX(y*(radar_xsize/xsize), 0), radar_ysize-radar_xsize/xsize);
		xGDALRasterIO(
				slp_band,
				GF_Read,
				0, slp_y,
				radar_xsize, radar_xsize/xsize,
				slp_buf,
				xsize, 1,
				GDT_Float32,
				0, 0);
		xGDALRasterIO(
				int_band,
				GF_Read,
				0, y,
				xsize, 1,
				int_buf,
				xsize, 1,
				GDT_CFloat32,
				0, 0);
		for (int x = 0; x < xsize; x++) {
			float phase;
			int slp_idx, int_idx;
			float h, s, v;
			phase = cargf(int_buf[x]);
			slp_idx = (slp_buf[x] - slp_min)*(255.)/(slp_max - slp_min);
			slp_idx = MIN(MAX(slp_idx, 0), 255);
			int_idx = (phase - int_min)*(255.)/(int_max - int_min);
			int_idx = MIN(MAX(int_idx, 0), 255);
			h = clut[3*int_idx];
			s = clut[3*int_idx+1];
			v = slp_idx;
			hsv2rgb(&tmp_buf_r[x], &tmp_buf_g[x], &tmp_buf_b[x],
					h, s, v);
		}
		xGDALRasterIO(
				tmp_band_r,
				GF_Write,
				0, y,
				xsize, 1, 
				tmp_buf_r,
				xsize, 1,
				GDT_Byte,
				0, 0);
		xGDALRasterIO(
				tmp_band_g,
				GF_Write,
				0, y,
				xsize, 1, 
				tmp_buf_g,
				xsize, 1,
				GDT_Byte,
				0, 0);
		xGDALRasterIO(
				tmp_band_b,
				GF_Write,
				0, y,
				xsize, 1, 
				tmp_buf_b,
				xsize, 1,
				GDT_Byte,
				0, 0);
	}
	free(slp_buf);
	free(int_buf);
	free(tmp_buf_r);
	free(tmp_buf_g);
	free(tmp_buf_b);

	/* Close input datasets */
	GDALClose(radar_ds);
	GDALClose(int_ds);

	/* Create the jpeg */
	dst_options = CSLSetNameValue(NULL, "QUALITY", "40");
	dst_ds =
		GDALCreateCopy(
				GDALGetDriverByName("jpeg"),
				argv[optind+2],
				tmp_ds,
				FALSE,
				dst_options,
				NULL, NULL);

	/* Close datasets */
	GDALClose(tmp_ds);
	GDALClose(dst_ds);

	/* Unlink tmp file again, it seems that gdal will reopen it at the first
	 * occasion */
	unlink(tmp_path);

	return EXIT_SUCCESS;
}

