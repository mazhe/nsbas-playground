/*************************************************************************
 *
 *  This file is part of NSBAS.
 *
 *  NSBAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NSBAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#include <cpl_string.h>
#include <gdal.h>

#include <nsbas/config.h>
#include <nsbas/xmalloc.h>
#include <nsbas/xgdal.h>

#include "hsv2rgb.h"

void
usage(void)
{
	fprintf(stderr,
			"usage:\n"
				"  nsb_preview_unw [-c color_table] [-m min_value] [-M max_value] radar_file unw_file out_file\n\n"
				"options:\n"
				"  -c color_table  apply a specific color lookup table (roipac, jet, bluered)\n"
				"  -m min_value    use this value as minimal for the value scaling\n"
				"  -M max_value    use this value as maximal for the value scaling\n"
				"\n");
}

extern const float clut_bluered[];
extern const float clut_jet[];
extern const float clut_roipac[];

int
main(int argc, char *argv[])
{
	char tmp_path[64] = "/tmp/nsb.XXXXXX";
	int tmp_fd;
	char **dst_options;
	GDALDatasetH radar_ds, unw_ds, tmp_ds, dst_ds;
	GDALRasterBandH slp_band, unw_band, tmp_band_r, tmp_band_g, tmp_band_b;
	double slp_min, slp_max, slp_mean, slp_stddev;
	double unw_min = NAN, unw_max = NAN, unw_mean, unw_stddev;
	int radar_xsize, radar_ysize, unw_xsize, unw_ysize, xsize, ysize;
	float *slp_buf, *unw_buf;
	uint8_t *tmp_buf_r, *tmp_buf_g, *tmp_buf_b;
	const float *clut = clut_roipac;

	/* Load GDAL drivers */
	GDALAllRegister();

	/* Read arguments */
	while (1) {
		int c = getopt(argc, argv, "c:m:M:h");
		if (c == -1) break;
		switch (c) {
		case 'c':
			if (strcmp(optarg, "bluered") == 0) {
				clut = clut_bluered;
			}
			else if (strcmp(optarg, "jet") == 0) {
				clut = clut_jet;
			}
			else if (strcmp(optarg, "roipac") == 0) {
				clut = clut_roipac;
			}
			else {
				fprintf(stderr,
						"Unknown color lookup table '%s': use bluered or roipac\n", optarg);
				exit(EXIT_FAILURE);
			}
			break;
		case 'm':
			unw_min = strtod(optarg, NULL);
			break;
		case 'M':
			unw_max = strtod(optarg, NULL);
			break;
		case 'h':
			usage();
			exit(EXIT_SUCCESS);
		case ':':
			fprintf(stderr,
					"Option -%c requires an operand\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		case '?':
			fprintf(stderr,
					"Unrecognized option: -%c\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		}
	}
	if (argc < optind+3) {
		usage();
		exit(EXIT_FAILURE);
	}

	/* Open relief dataset */
	radar_ds = xGDALOpen(argv[optind], GA_ReadOnly);
	slp_band = GDALGetRasterBand(radar_ds, 1);
	GDALGetRasterStatistics(
			slp_band,
			1, 1,
			&slp_min, &slp_max, &slp_mean, &slp_stddev);
	slp_min = slp_mean - slp_stddev*2.0;
	slp_max = slp_mean + slp_stddev*2.0;

	/* Open unw dataset */
	unw_ds = xGDALOpen(argv[optind+1], GA_ReadOnly);
	unw_band = GDALGetRasterBand(unw_ds, GDALGetRasterCount(unw_ds));
	GDALGetRasterStatistics(
			unw_band,
			0, 1,
			NULL, NULL, &unw_mean, &unw_stddev);
	if (isnan(unw_min)) {
		unw_min = unw_mean - unw_stddev*2.0;
	}
	if (isnan(unw_max)) {
		unw_max = unw_mean + unw_stddev*2.0;
	}

	/* Get sizes for output dataset */
	radar_xsize = GDALGetRasterXSize(radar_ds);
	radar_ysize = GDALGetRasterYSize(radar_ds);
	unw_xsize = GDALGetRasterXSize(unw_ds);
	unw_ysize = GDALGetRasterYSize(unw_ds);
	xsize = unw_xsize;
	ysize = MIN(unw_ysize, radar_xsize/unw_xsize*radar_ysize);

	/* Create temp dataset */
	tmp_fd = mkstemp(tmp_path);
	if (tmp_fd == -1) {
		fprintf(stderr, "cannot create tmp file\n");
		exit(EXIT_FAILURE);
	}
	unlink(tmp_path);
	tmp_ds = 
		GDALCreate(
				GDALGetDriverByName("gtiff"),
				tmp_path,
				xsize, ysize, 3,
				GDT_Byte,
				NULL);
	if (tmp_ds == NULL) {
		fprintf(stderr, "cannot open %s\n", argv[3]);
		exit(EXIT_FAILURE);
	}
	close(tmp_fd);
	tmp_band_r = GDALGetRasterBand(tmp_ds, 1);
	tmp_band_g = GDALGetRasterBand(tmp_ds, 2);
	tmp_band_b = GDALGetRasterBand(tmp_ds, 3);

	/* Do the stuff */
	slp_buf = xmalloc(xsize*sizeof(float));
	unw_buf = xmalloc(xsize*sizeof(float));
	tmp_buf_r = xmalloc(xsize*sizeof(uint8_t));
	tmp_buf_g = xmalloc(xsize*sizeof(uint8_t));
	tmp_buf_b = xmalloc(xsize*sizeof(uint8_t));
	for (int y = 0; y < ysize; y++) {
		int slp_y = MIN(MAX(y*radar_xsize/xsize, 0), radar_ysize-radar_xsize/xsize);
		xGDALRasterIO(
				slp_band,
				GF_Read,
				0, slp_y,
				radar_xsize, radar_xsize/xsize,
				slp_buf,
				xsize, 1,
				GDT_Float32,
				0, 0);
		xGDALRasterIO(
				unw_band,
				GF_Read,
				0, y,
				xsize, 1,
				unw_buf,
				xsize, 1,
				GDT_Float32,
				0, 0);
		for (int x = 0; x < xsize; x++) {
			int slp_idx, unw_idx;
			float h, s, v;
			slp_idx = (slp_buf[x] - slp_min)*(255.)/(slp_max - slp_min);
			slp_idx = MIN(MAX(slp_idx, 0), 255);
			unw_idx = (unw_buf[x] - unw_min)*(255.)/(unw_max - unw_min);
			unw_idx = MIN(MAX(unw_idx, 0), 255);
			h = clut[3*unw_idx];
			s = (unw_buf[x] != 0.f)	? clut[3*unw_idx+1] : 0.f;
			v = slp_idx;
			hsv2rgb(&tmp_buf_r[x], &tmp_buf_g[x], &tmp_buf_b[x],
					h, s, v);
		}
		xGDALRasterIO(
				tmp_band_r,
				GF_Write,
				0, y,
				xsize, 1, 
				tmp_buf_r,
				xsize, 1,
				GDT_Byte,
				0, 0);
		xGDALRasterIO(
				tmp_band_g,
				GF_Write,
				0, y,
				xsize, 1, 
				tmp_buf_g,
				xsize, 1,
				GDT_Byte,
				0, 0);
		xGDALRasterIO(
				tmp_band_b,
				GF_Write,
				0, y,
				xsize, 1, 
				tmp_buf_b,
				xsize, 1,
				GDT_Byte,
				0, 0);
	}
	free(slp_buf);
	free(unw_buf);
	free(tmp_buf_r);
	free(tmp_buf_g);
	free(tmp_buf_b);

	/* Close input datasets */
	GDALClose(radar_ds);
	GDALClose(unw_ds);

	/* Create the jpeg */
	dst_options = CSLSetNameValue(NULL, "QUALITY", "40");
	dst_ds =
		GDALCreateCopy(
				GDALGetDriverByName("jpeg"),
				argv[optind+2],
				tmp_ds,
				FALSE,
				dst_options,
				NULL, NULL);

	/* Close datasets */
	GDALClose(tmp_ds);
	GDALClose(dst_ds);

	/* Unlink tmp file again, it seems that gdal will reopen it at the first
	 * occasion */
	unlink(tmp_path);

	return EXIT_SUCCESS;
}

