#!@PYTHON_EXECUTABLE@
# -*- coding: utf-8 -*-
##########################################################################
# 
#   This file is part of NSBAS.
# 
#   NSBAS is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   NSBAS is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
# 
##########################################################################

import glob, sys, os

from efidir.sws import *

def make_jpeg_slc(f):
    outputs = [ os.path.basename(f)+".jpeg" ]
    inputs = [ f ]
    commands = [
        "~volatm/utils/bin/nsb_preview_slc "+inputs[0]+" "+outputs[0]
    ]
    return target_create(commands, inputs, outputs)

def make_jpeg_int(f):
    outputs = [ os.path.basename(f)+".jpeg" ]
    inputs = [ f ]
    commands = [
        "~volatm/utils/bin/nsb_preview_int radar.hgt "+inputs[0]+" "+outputs[0]
    ]
    return target_create(commands, inputs, outputs)

def make_jpeg_unw(f):
    outputs = [ os.path.basename(f)+".jpeg" ]
    inputs = [ f ]
    commands = [
        "~volatm/utils/bin/nsb_preview_unw radar.hgt "+inputs[0]+" "+outputs[0]
    ]
    return target_create(commands, inputs, outputs)

# Targets to process (will be filled later)
targets = []

# Input files
if len(sys.argv) == 2:
    files = glob.glob(sys.argv[1])
else:
    files = sys.argv[1:]

# Check the filetype is unique
ext = os.path.splitext(files[0])[1]
for f in files:
    if os.path.splitext(f)[1] != ext:
        raise Exception("Mixed files types")

# Iterate over each file to add them to the task list
for f in files:
    if ext == ".slc":
        target = make_jpeg_slc(f)

    elif ext == ".int":
        target = make_jpeg_int(f)

    elif ext == ".unw":
        target = make_jpeg_unw(f)

    targets.append(target)

# Execute the work
chain_run(chain_create("make_jpegs", targets))
