/*************************************************************************
 *
 *  This file is part of NSBAS.
 *
 *  NSBAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NSBAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

#include <complex.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#ifdef __linux__
# include <getopt.h>
#endif

#include <gdal.h>

#include <nsbas/config.h>
#include <nsbas/xmalloc.h>
#include <nsbas/xgdal.h>

static void
usage(void)
{
	fprintf(stderr, "usage: nsb_addphase [-f format] [-c coeff] source_file_a source_file_b destination_file\n");
}

int
main(int argc, char *argv[])
{
	const char *format = NSBAS_DEFAULT_FORMAT;
	GDALDatasetH a_ds, b_ds, dst_ds;
	GDALRasterBandH a_band, b_band, dst_band;
	int dst_xsize, dst_ysize;
	complex float *a_buf, *b_buf, *dst_buf;
	float coeff = 1.f;
	
	/* Load GDAL drivers */
	GDALAllRegister();
	
	/* Read arguments */
	while (1) {
		int c = getopt(argc, argv, "f:c:h");
		if (c == -1) break;
		switch (c) {
		case 'f':
			format = optarg;
			break;
		case 'c':
			coeff = strtod(optarg, NULL);
			break;
		case 'h':
			usage();
			exit(EXIT_SUCCESS);
		case ':':
			fprintf(stderr,
					"Option -%c requires an operand\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		case '?':
			fprintf(stderr,
					"Unrecognized option: -%c\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		}
	}
	if (argc < optind+3) {
		exit(EXIT_FAILURE);
	}

	/* Open source datasets */
	a_ds = xGDALOpen(argv[optind], GA_ReadOnly);
	a_band = GDALGetRasterBand(a_ds, 1);

	b_ds = xGDALOpen(argv[optind+1], GA_ReadOnly);
	b_band = GDALGetRasterBand(b_ds, 1);

	/* Compute sizes for output dataset */
	dst_xsize = GDALGetRasterXSize(a_ds);
	dst_ysize = GDALGetRasterYSize(a_ds);

	/* Create output dataset */
	dst_ds = 
		GDALCreate(
				GDALGetDriverByName(format),
				argv[optind+2],
				dst_xsize, dst_ysize, 1,
				GDT_CFloat32,
				NULL);
	if (dst_ds == NULL) {
		exit(EXIT_FAILURE);
	}
	dst_band = GDALGetRasterBand(dst_ds, 1);

	/* Do the downsampling */
	a_buf = xmalloc(dst_xsize*sizeof(complex float));
	b_buf = xmalloc(dst_xsize*sizeof(complex float));
	dst_buf = xmalloc(dst_xsize*sizeof(complex float));
	for (int y = 0; y < dst_ysize; y++) {
		xGDALRasterIO(
				a_band,
				GF_Read,
				0, y,
				dst_xsize, 1,
				a_buf,
				dst_xsize, 1,
				GDT_CFloat32,
				0, 0);
		xGDALRasterIO(
				b_band,
				GF_Read,
				0, y,
				dst_xsize, 1,
				b_buf,
				dst_xsize, 1,
				GDT_CFloat32,
				0, 0);

		for (int x = 0; x < dst_xsize; x++) {
			float amp, a_phase, b_phase;
			amp = cabsf(a_buf[x]);
			a_phase = cargf(a_buf[x]);
			b_phase = cargf(b_buf[x]);
			dst_buf[x] = amp*cexpf(I*(a_phase + coeff*b_phase));
		}

		xGDALRasterIO(
				dst_band,
				GF_Write,
				0, y,
				dst_xsize, 1, 
				dst_buf,
				dst_xsize, 1,
				GDT_CFloat32,
				0, 0);
	}
	free(a_buf);
	free(b_buf);
	free(dst_buf);

	/* Close datasets */
	GDALClose(a_ds);
	GDALClose(b_ds);
	GDALClose(dst_ds);

	return EXIT_SUCCESS;
}
