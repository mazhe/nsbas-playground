/*************************************************************************
 *
 *  This file is part of NSBAS.
 *
 *  NSBAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NSBAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NSBAS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

#include <complex.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <gdal.h>
#include <cpl_string.h>

#include <nsbas/xmalloc.h>
#include <nsbas/xgdal.h>

static void
usage(void)
{
	fprintf(stderr, "usage: nsb_look [-f format] [-a azimuth_look] [-r range_look] [-l look_function] source_file destination_file\n");
}

static inline void
look_cplx(complex float *data, int xsize, int alooks, int rlooks)
{
	for (int x = 0; x < xsize; x++) {
		complex float z = data[x];
		for (int i = 1; i < alooks; i++) {
			z += data[i*xsize + x];
		}
		data[x] = z;
	}
	for (int x = 0; x < xsize/rlooks; x++) {
		complex float z = data[x*rlooks];
		for (int i = 1; i < rlooks; i++) {
			z += data[x*rlooks + i];
		}
		data[x] = z;
	}
}

static inline void
look_pow(complex float *data, int xsize, int alooks, int rlooks)
{
    for (int x = 0; x < xsize; x++) {
        float z = cabsf(data[x]) * cabsf(data[x]);
        for (int i = 1; i < alooks; i++) {
            float f = cabsf(data[i*xsize + x]);
            z += f*f;
        }
        data[x] = z;
    }
    for (int x = 0; x < xsize/rlooks; x++) {
        float z = crealf(data[x*rlooks]);
        for (int i = 1; i < rlooks; i++) {
            z += crealf(data[x*rlooks + i]);
        }
        data[x] = z;
    }
}

static inline void
look_ri(complex float *data, int xsize, int alooks, int rlooks)
{
	for (int x = 0; x < xsize; x++) {
		complex float z = data[x] * data[x];
		for (int i = 1; i < alooks; i++) {
			z += data[i*xsize + x] * data[i*xsize + x];
		}
		data[x] = z;
	}
	for (int x = 0; x < xsize/rlooks; x++) {
		complex float z = data[x*rlooks];
		for (int i = 1; i < rlooks; i++) {
			z += data[x*rlooks + i];
		}
		data[x] = sqrtf(z);
	}
}

int
main(int argc, char *argv[])
{
	const char *format = "roi_pac";
	void (*look_func)(complex float *, int, int, int) = NULL;
	int alooks = 4, rlooks = 0;
	GDALDatasetH src_ds, dst_ds;
	GDALRasterBandH src_band, dst_band;
	int src_xsize, src_ysize, dst_xsize, dst_ysize, count;
	complex float *buf;
	char **md;
	
	/* Load GDAL drivers */
	GDALAllRegister();
	
	/* Read arguments */
	while (1) {
		int c = getopt(argc, argv, "f:a:r:l:h");
		if (c == -1) break;
		switch (c) {
		case 'f':
			format = optarg;
			break;
		case 'a':
			alooks = atoi(optarg);
			break;
		case 'r':
			rlooks = atoi(optarg);
			break;
		case 'l':
			if (strcmp(optarg, "cplx") == 0) {
				look_func = look_cplx;
			} 
			else if (strcmp(optarg, "pow") == 0) {
				look_func = look_pow;
			}
			else if (strcmp(optarg, "ri") == 0) {
				look_func = look_ri;
			}
			else {
				fprintf(stderr,
						"Invalid look function: %s\n", optarg);
				usage();
				exit(EXIT_FAILURE);
			}
			break;
		case 'h':
			usage();
			exit(EXIT_SUCCESS);
		case ':':
			fprintf(stderr,
					"Option -%c requires an operand\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		case '?':
			fprintf(stderr,
					"Unrecognized option: -%c\n", optopt);
			usage();
			exit(EXIT_FAILURE);
		}
	}
	if (argc < optind+1) {
		exit(EXIT_FAILURE);
	}

	if (rlooks == 0) {
		rlooks = alooks;
	}

	/* Open source dataset */
	src_ds = xGDALOpen(argv[optind], GA_ReadOnly);

	/* If no look function specified, choose one from data type */
	if (look_func == NULL) {
		switch (GDALGetRasterDataType(GDALGetRasterBand(src_ds, 1))) {
		case GDT_CInt16:
		case GDT_CInt32:
		case GDT_CFloat32:
		case GDT_CFloat64:
			look_func = look_cplx;
			break;
		default:
			look_func = look_pow;
			break;
		}
	}

	/* Compute sizes for output dataset */
    count = GDALGetRasterCount(src_ds);
	src_xsize = GDALGetRasterXSize(src_ds);
	src_ysize = GDALGetRasterYSize(src_ds);
	dst_xsize = src_xsize / rlooks;
	dst_ysize = src_ysize / alooks;

	/* Create output dataset */
	dst_ds = 
		GDALCreate(
				GDALGetDriverByName(format),
				argv[optind+1],
				dst_xsize, dst_ysize, count,
				GDALGetRasterDataType(GDALGetRasterBand(src_ds, 1)),
				NULL);
	if (dst_ds == NULL) {
		exit(EXIT_FAILURE);
	}

	/* Do the downsampling */
	buf = xmalloc(src_xsize*alooks*sizeof(complex float));
	for (int b = 1; b <= count; b++) {
		src_band = GDALGetRasterBand(src_ds, b);
		dst_band = GDALGetRasterBand(dst_ds, b);
		for (int y = 0; y < dst_ysize; y++) {
			xGDALRasterIO(
					src_band,
					GF_Read,
					0, y*alooks,
					src_xsize, alooks,
					buf,
					src_xsize, alooks,
					GDT_CFloat32,
					0, 0);
			
			look_func(buf, src_xsize, alooks, rlooks);

			xGDALRasterIO(
					dst_band,
					GF_Write,
					0, y,
					dst_xsize, 1, 
					buf,
					dst_xsize, 1,
					GDT_CFloat32,
					0, 0);
		}
	}
	free(buf);

	/* Copy/update the metadata */
	/* TODO: this is not the most readable part of the program... We should
	 *       write helper functions */
	if ((md = GDALGetMetadata(src_ds, "ROI_PAC")) != NULL) {
		const char *val;
		char newval[32];

		if ((val = CSLFetchNameValue(md, "XMIN")) != NULL) {
			snprintf(newval, sizeof(newval),
					"%ld", strtol(val, NULL, 10)/rlooks);
			md = CSLSetNameValue(md, "XMIN", newval);
		}

		if ((val = CSLFetchNameValue(md, "XMAX")) != NULL) {
			snprintf(newval, sizeof(newval),
					"%ld", strtol(val, NULL, 10)/rlooks);
			md = CSLSetNameValue(md, "XMIN", newval);
		}

		if ((val = CSLFetchNameValue(md, "YMIN")) != NULL) {
			snprintf(newval, sizeof(newval),
					"%ld", strtol(val, NULL, 10)/alooks);
			md = CSLSetNameValue(md, "YMIN", newval);
		}

		if ((val = CSLFetchNameValue(md, "YMAX")) != NULL) {
			snprintf(newval, sizeof(newval),
					"%ld", strtol(val, NULL, 10)/alooks);
			md = CSLSetNameValue(md, "YMAX", newval);
		}

		if ((val = CSLFetchNameValue(md, "DELTA_LINE_UTC")) != NULL) {
			snprintf(newval, sizeof(newval),
					"%ld", strtol(val, NULL, 10)*alooks);
			md = CSLSetNameValue(md, "DELTA_LINE_UTC", newval);
		}

		if ((val = CSLFetchNameValue(md, "RANGE_PIXEL_SIZE")) != NULL) {
			snprintf(newval, sizeof(newval),
					"%ld", strtol(val, NULL, 10)*rlooks);
			md = CSLSetNameValue(md, "RANGE_PIXEL_SIZE", newval);
		}

		if ((val = CSLFetchNameValue(md, "AZIMUTH_PIXEL_SIZE")) != NULL) {
			snprintf(newval, sizeof(newval),
					"%ld", strtol(val, NULL, 10)*alooks);
			md = CSLSetNameValue(md, "AZIMUTH_PIXEL_SIZE", newval);
		}

		if ((val = CSLFetchNameValue(md, "X_STEP")) != NULL) {
			snprintf(newval, sizeof(newval),
					"%ld", strtol(val, NULL, 10)*rlooks);
			md = CSLSetNameValue(md, "X_STEP", newval);
		}

		if ((val = CSLFetchNameValue(md, "Y_STEP")) != NULL) {
			snprintf(newval, sizeof(newval),
					"%ld", strtol(val, NULL, 10)*alooks);
			md = CSLSetNameValue(md, "Y_STEP", newval);
		}

		GDALSetMetadata(dst_ds, md, "ROI_PAC");
	}

	/* Close datasets */
	GDALClose(src_ds);
	GDALClose(dst_ds);

	return EXIT_SUCCESS;
}
